console.log("Hello World");

let trainer = {
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	name: "Ash Ketchum",
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function () {
		console.log(trainer.pokemon[0] + "! I choose you!");
	}
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();


function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function (target) {
		let remainingHealth = Number(target.health) - this.attack
		target.health = remainingHealth;
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + remainingHealth);
	}

	this.faint = function () {

		if (this.health <= 0) {
			console.log(this.name + " fainted.");
		}
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewTwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewTwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewTwo.tackle(geodude);
geodude.faint();
console.log(geodude);